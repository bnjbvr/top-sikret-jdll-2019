JDLL 2019 - Contrib'ateliers
===

Source de nos slides pour la conférence sur les Contrib'Ateliers aux Journées
du logiciel libre à Lyon de 2019.

[Slides rendus](https://bnjbvr.frama.io/top-sikret-jdll-2019/).
